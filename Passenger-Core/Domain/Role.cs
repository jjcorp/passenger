﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Passengers.Core.Domain
{
   public  class Role
    {
        public static string User => "User";
        public static string Driver => "Driver";
        public static string Passenger => "Passenger";
    }
}
